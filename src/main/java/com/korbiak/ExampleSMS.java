package com.korbiak;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

public class ExampleSMS {
    // Find your Account Sid and Token at twilio.com/user/account
    public static final String ACCOUNT_SID = "AC3e0abef9a86ce876082c2d2eccfa61ff";
    public static final String AUTH_TOKEN = "e626cf3bb29d26604ed109cb428b9c52";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message.creator(new PhoneNumber("+380980265122"), /*my phone number*/
                new PhoneNumber("+12028664738"), str).create(); /*attached to me number*/
    }
}
